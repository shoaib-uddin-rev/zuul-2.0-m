import { NgModule } from '@angular/core';
import { CommonModule, Location } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { ContactpopoverPageComponent } from './contactpopover-page/contactpopover-page.component';
import { GmapPageComponent } from '../pages/gmap-page/gmap-page.component';
import { EmptyviewComponentModule } from './emptyview/emptyview.component.module';


@NgModule({
    declarations: [
        ContactpopoverPageComponent,
        GmapPageComponent,
    ],
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        IonicModule,
        EmptyviewComponentModule
    ],
    exports: [
        ContactpopoverPageComponent,
        GmapPageComponent,
        EmptyviewComponentModule
    ],

})
export class SharedModule { }