import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { IonicModule } from '@ionic/angular';
import { TutorialCardsComponent } from './tutorial-cards.component';

@NgModule({
	declarations: [
		TutorialCardsComponent
	],
	imports: [
		IonicModule
	],
	exports: [
		TutorialCardsComponent
	]
})
export class TutorialCardsComponentModule {}