import { Component, Injector, OnInit } from '@angular/core';
import { AnnouncementPage } from '../announcement/announcement.page';
import { BasePage } from '../base-page/base-page';

import { ContactpopoverPageComponent } from '../../components/contactpopover-page/contactpopover-page.component';
import { MypassesPage } from '../mypasses/mypasses.page';
import { MypassesdetailPage } from '../mypassesdetail/mypassesdetail.page';
import { NotificationsPage } from '../notifications/notifications.page';
import { ParentalLogsPage } from '../parental-logs/parental-logs.page';
import { RequestAPassPageComponent } from '../request-apass-page/request-apass-page.component';
import { UpdatePasswordPage } from '../update-password/update-password.page';
import { VManageVehiclesPage } from '../v-manage-vehicles/v-manage-vehicles.page';
import { NewPassPage } from '../new-pass/new-pass.page';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.page.html',
  styleUrls: ['./dashboard.page.scss'],
})
export class DashboardPage extends BasePage implements OnInit {

  avatar = this.users.avatar;
  canBeResident = false;
  isUserLoaded = false;
  user: any;
  plist: any[] = [];
  storedData = {
    page: 1,
    expired: 0
  };
  rnotif = false;
  // contact_count = 0;
  getNotified = false;
  isEmailVerificationPending = false;
  suspandAccount = false;
  disableCreatePass = true;

  notificationNumber = -1;

  constructor(
    injector: Injector
  ) {
    super(injector);
    this.initializeView();
    // this.events.unsubscribe('user:shownotification');
    // this.events.subscribe('user:shownotification', this.notificationReceived.bind(this));
    // this.events.unsubscribe('user:redirectToCreatePass');
    // this.events.subscribe('user:redirectToCreatePass', this.mypassThenNewPass.bind(this));

  }

  ngOnInit() {

  }

  async initializeView() {

    // let params = this.nav.getParams();
    // let showelcome = params["showelcome"];
    // var self = this;
    this.user = await this.sqlite.getActiveUser();
    // console.log("this. user The here", this.user);
    // this.users._user = this.user;

    // this.canBeResident = (parseInt(this.user["can_user_become_resident"]) == 1);
    // this.suspandAccount = (parseInt(this.user['suspand']) == 1) ? true : false;
    // this.disableCreatePass = !(parseInt(this.user["can_send_passes"]) == 1);

    this.menuCtrl.enable(true, 'authenticated');
    // sync contact list from global database to local database,
    // if sync already done skip the step

    // await this.users.getContactDatabseOfUser(this.user);

    this.getPasses(this.storedData).then(() => {

      // if (showelcome == true) {
      //   this.nav.push('RegistrationPage', { new: true, user: this.user })
      // } else
      //   if (this.user.is_reset_password == 1) {
      //     let myModal21 = this.modals.present(UpdatePasswordPage, { user: this.user });
      //   }
      //   else if (!this.validateProfile(this.user)) {
      //     this.nav.push('RegistrationPage', { new: true, user: this.user })
      //   }
      //   else {
      //     self.callAnnouncements();
      //   }
    });
    this.users.isUserEmailPendingVerification(this.user).then(count => {
      this.isEmailVerificationPending = count > 0 ? true : false;
    });
    this.events.publish('user:settokentoserver');

    // this.network.getVendorList().subscribe( async v => {
    //   await this.sqlite.setVendorListInDatabase(v["vendors"]);
    // })

    if (this.suspandAccount == true) {
      this.utility.presentFailureToast("Your Account Is Suspended, Please Contact You Admin ");
    }


  }




  ionViewWillEnter() {
    this.events.subscribe('dashboard:carselection', this.openVehicleSelection.bind(this));
  }

  ionViewWillLeave() {
    // this.events.unsubscribe('user:shownotification');
    // this.events.unsubscribe('user:redirectToCreatePass');
    this.events.unsubscribe('dashboard:carselection');
  }

  openVehicleSelection() {
    // open vehicle modal with CRUD
    let myModal = this.modals.present(VManageVehiclesPage);
    // myModal.onDidDismiss(data => {
    //   // console.log(data);
    // });
    // myModal.present();
  }

  validateProfile(user) {

    // console.log(user);

    if (user.name == "" || !user.name
      || user.profile.last_name == "" || !user.profile.last_name
      || user.profile.date_of_birth == "" || !user.profile.date_of_birth
      || user.phone_number == "" || !user.phone_number
      || user.dial_code == "" || !user.dial_code
      || user.email == "" || !user.email
      || user.profile.street_address == "" || !user.profile.street_address
      || user.profile.city == "" || !user.profile.city
      || user.profile.state == "" || !user.profile.state
      || user.profile.zip_code == "" || !user.profile.zip_code
    ) {
      return false;
    }
    return true;
  }


  async notificationReceived(data) {
    console.log(data);
    this.getNotified = true;
    this.utility.presentToast(data.title);
    await this.emptyarray();
    this.storedData.page = 0;

    await this.getPasses(this.storedData, false, true);


    // if(this.notification_number == -1){
    //   this.notification_number = 1;
    //   this.doRefresh().then( () => {
    //     this.notification_number = -1;
    //   });

    // }
    //


    // this.page = 0;
    // this.plist = [];
    // this.getPasses(this.storedData);



    // if (data.hasOwnProperty('body')) {

    //   // this.rnotif = true;

    //   // this.utility.showAlert(data.body);
    // }



  }

  emptyarray() {
    var self = this;
    return new Promise<void>(resolve => {
      self.plist = [];
      resolve();
    })
  }

  presentDuplicateEmailVerification(ev) {
    this.nav.push('CodeVerificationPage', { emv: true });
  }

  presentPopover(ev) {

    this.getNotified = false;
    // this.users.sendNotification().subscribe((data: any) => {
    //   // console.log(data);
    // })
    this.rnotif = false;
    let myModal = this.modals.present(NotificationsPage);
    // myModal.onDidDismiss(data => {
    //   // console.log(data);
    // });
    // myModal.present();

  }

  presentPopoverParental() {
    this.getNotified = false;
    // this.users.sendNotification().subscribe((data: any) => {
    //   // console.log(data);
    // })
    this.rnotif = false;
    let myModal = this.modals.present(ParentalLogsPage);
    // myModal.onDidDismiss(data => {
    //   // console.log(data);
    // });
    // myModal.present();
  }

  callAnnouncements() {

    return new Promise<void>(resolve => {
      this.network.getUnreadAnnouncements().then(data => {
        // console.log(data);
        var d: any[] = data.list;
        if (d.length > 0) {
          let myModal = this.modals.present(AnnouncementPage, { plist: d });
          // myModal.present();

        }

        resolve();

      }, err => { });
    })


  }

  doRefresh(refresher = null) {

    this.notificationNumber = -1;

    return new Promise<void>(async resolve => {
      this.plist = [];
      this.storedData.page = 0;

      await this.getPasses(this.storedData);
      // await this.callAnnouncements();
      if (refresher) {
        refresher.complete();
      }

      resolve();
    })


  }

  getPasses(data, loader = true, fromNotification = false) {

    return new Promise<void>((resolve) => {

      if (this.storedData.page === -1) {
        resolve();
      } else {
        // data.expired = 0;
        // data.page = this.storedData.page;
        // data['quickpass'] = fromNotification === true ? 0 : 1;
        this.network.getUserReceivedPasses(this.user.id, data, loader).then((res: any) => {
          console.log(res);
          // if (fromNotification) {
          //   this.plist = res.list;
          //   res.page = 0;
          // } else {
            if (this.storedData.page == 0) {
              this.plist = res.list;
            } else {
              this.plist = this.plist.concat(res.list);
            }

            this.storedData.page = this.storedData.page + 1;
            resolve();
        });

      }

    })
  }

  becomeAResident() {
    this.nav.push('CodeVerificationPage');
  }

  mycontactbutton() {
    // MyContactPage

    this.nav.push('pages/contacts', {
      animate: true,
      direction: 'forward'
    });
  }

  mypass() {

    let myModal = this.modals.present(MypassesPage, { arc: false });
    // // let myModal = this.modalCtrl.create(VPassesPage, { arc: false });
    // myModal.onDidDismiss(data => {
    //   // console.log(data);
    // });
    // myModal.present();

  }

  mypassThenNewPass(item, ease) {

    // console.log(item, ease);
    let myModal = this.modals.present(NewPassPage, { rap: true, group: false, item: item });

    // myModal.onDidDismiss(data => {
    //   // console.log(data);
    // });
    // myModal.present();

  }

  async myNewPass() {
    const data = await this.modals.present(NewPassPage);
    // myModal.onDidDismiss(data => {
    //   // console.log(data);
    // });
    // myModal.present();
  }

  myArchpass() {
    const data = this.modals.present(MypassesPage, { arc: true });
    // myModal.onDidDismiss(data => {
    //   // console.log(data);
    // });
    // myModal.present();
  }

  requestapass() {
    let myModal = this.modals.present(RequestAPassPageComponent);
    // myModal.onDidDismiss(data => {
    //   // console.log(data);
    // });
    // myModal.present();
  }

  logout() {
    this.events.publish('user:logout');
  }

  openFamily() {
    this.nav.push('MyfamilymembersPage', {
      animate: true,
      direction: 'forward'
    });
  }

  passDetails(sel) {
    this.nav.push(MypassesdetailPage, { item: sel.qrcode_id, flag: "active" });
  }

  openLogUsers(myEvent) {
    // var r = item;
    var self = this;
    let popover2 = this.popover.present(ContactpopoverPageComponent, {
      id: null,
      flag: "SW"
    });

    // popover2.(data => {
    //   // let tr = data;
    //   if (data == null) {
    //     return;
    //   }

    //   var sw_user = data["param"];
    //   // console.log(sw_user);
    //   this.users.switchUserAccount(sw_user['id']).then(data => {
    //     if (!data) {
    //       this.nav.push('LoginPage', { sw_user: sw_user });
    //     }
    //   })

    // });
  }

  openDoubleBell(myEvent) {
    // var r = item;
    var self = this;

    this.getNotified = false;
    this.rnotif = false;

    let popover2 = this.popover.present( ContactpopoverPageComponent , {
      id: null,
      flag: "PCN"
    });
    // popover2.present({
    //   ev: myEvent
    // });

    // popover2.onDidDismiss(data => {
    //   // let tr = data;
    //   if (data == null) {
    //     return;
    //   }

    //   var item = data["pid"];
    //   switch (data["param"]) {
    //     case "P":
    //       let myModal = this.modalCtrl.create(ParentalLogsPage);
    //       myModal.present();
    //       break;
    //     case "N":
    //       let myModal2 = this.modalCtrl.create(NotificationsPage);
    //       myModal2.present();
    //       break;
    //   }

    // });
  }

  loadMore($event) {

    this.getPasses(this.storedData, false).then(v => {
      $event.complete();
    });


  }

}

