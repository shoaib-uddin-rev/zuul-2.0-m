import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { CommonModule, Location } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { PagesRoutingModule } from './pages-routing.module';
import { EmptyviewComponentModule } from '../components/emptyview/emptyview.component.module';


@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        IonicModule,
        PagesRoutingModule,
        EmptyviewComponentModule
    ],
    providers: [
        Location,
    ]

})
export class PagesModule { }
