import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SyncContactsStatusPageRoutingModule } from './sync-contacts-status-routing.module';

import { SyncContactsStatusPage } from './sync-contacts-status.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SyncContactsStatusPageRoutingModule
  ],
  declarations: [SyncContactsStatusPage]
})
export class SyncContactsStatusPageModule {}
