import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SyncContactsStatusPage } from './sync-contacts-status.page';

const routes: Routes = [
  {
    path: '',
    component: SyncContactsStatusPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SyncContactsStatusPageRoutingModule {}
