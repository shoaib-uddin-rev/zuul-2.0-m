import { Component, Injector, Input, OnInit } from '@angular/core';
import { BasePage } from '../base-page/base-page';

@Component({
  selector: 'app-sync-contacts-status',
  templateUrl: './sync-contacts-status.page.html',
  styleUrls: ['./sync-contacts-status.page.scss'],
})
export class SyncContactsStatusPage extends BasePage implements OnInit {

  @Input() phone_contacts: any[] = [];
  @Input() title = "Sync Contact"

  constructor(injector: Injector) { 
    super(injector)
  }

  ngOnInit() {}

  close(){
    this.modals.dismiss();
  }

}
