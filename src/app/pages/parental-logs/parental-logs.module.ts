import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ParentalLogsPageRoutingModule } from './parental-logs-routing.module';

import { ParentalLogsPage } from './parental-logs.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ParentalLogsPageRoutingModule
  ],
  declarations: [ParentalLogsPage]
})
export class ParentalLogsPageModule {}
