import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MypassesdetailPageRoutingModule } from './mypassesdetail-routing.module';

import { MypassesdetailPage } from './mypassesdetail.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MypassesdetailPageRoutingModule
  ],
  declarations: [MypassesdetailPage]
})
export class MypassesdetailPageModule {}
