import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MypassesdetailPage } from './mypassesdetail.page';

const routes: Routes = [
  {
    path: '',
    component: MypassesdetailPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MypassesdetailPageRoutingModule {}
