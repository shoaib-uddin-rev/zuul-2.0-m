import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SyncContactsPageRoutingModule } from './sync-contacts-routing.module';

import { SyncContactsPage } from './sync-contacts.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SyncContactsPageRoutingModule
  ],
  declarations: [SyncContactsPage]
})
export class SyncContactsPageModule {}
