import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SyncContactsPage } from './sync-contacts.page';

const routes: Routes = [
  {
    path: '',
    component: SyncContactsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SyncContactsPageRoutingModule {}
