import { Component, OnInit, Injector, Input, AfterViewInit } from '@angular/core';
import { BasePage } from '../../base-page/base-page';

@Component({
  selector: 'app-pass-guest-logs-page',
  templateUrl: './pass-guest-logs-page.component.html',
  styleUrls: ['./pass-guest-logs-page.component.scss'],
})
export class PassGuestLogsPageComponent extends BasePage implements OnInit, AfterViewInit {

  items: any = [];
  _items: any = [];

  @Input() pass_id;
  @Input() type;

  constructor(injector: Injector) { 
    super(injector);

   

  }

  ngAfterViewInit() {
    this.network.getPassScanlogs(this.pass_id, {type: this.type}).then( v => {
      if(v['bool'] == true){
        this.items = v['scanlogs']
        this._items = v['scanlogs']
      }
    })
  }

  ngOnInit() {}

  setFilteredItems(searchTerm, flag) {

    this.items = this._items.filter(item => {

        return item.fullNameInverted.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1
        || item.phone_number.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1
        || item.scanByFullName.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1
        || item.scanDate.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1

    });

  }

  closeModal(res){
    this.modals.dismiss(res);
  }

}
