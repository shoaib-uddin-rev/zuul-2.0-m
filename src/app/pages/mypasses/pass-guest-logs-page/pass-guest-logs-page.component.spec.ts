import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PassGuestLogsPageComponent } from './pass-guest-logs-page.component';

describe('PassGuestLogsPageComponent', () => {
  let component: PassGuestLogsPageComponent;
  let fixture: ComponentFixture<PassGuestLogsPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PassGuestLogsPageComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PassGuestLogsPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
