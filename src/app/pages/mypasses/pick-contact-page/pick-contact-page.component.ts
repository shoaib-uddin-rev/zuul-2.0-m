import { Component, OnInit, Injector } from '@angular/core';
import { BasePage } from '../../base-page/base-page';

@Component({
  selector: 'app-pick-contact-page',
  templateUrl: './pick-contact-page.component.html',
  styleUrls: ['./pick-contact-page.component.scss'],
})
export class PickContactPageComponent extends BasePage implements OnInit {

  user: any;
  storedData;
  phone_contacts: any[] = [];

  constructor(injector: Injector) { 
    super(injector);
  }

  ngOnInit() {}

  getMyContacts(){

    this.network.getContacts().then((response: any) =>{

      this.phone_contacts = response['contact_list'];
      this.phone_contacts.forEach((item) => {
        item['checked'] = false;
      });

    }, err => {});
  }

  closeModal() {

    var fi1 = this.phone_contacts.filter(
      itm => itm.selected == true);
    // console.log(fi1);
    this.modals.dismiss(fi1);

  }

}
