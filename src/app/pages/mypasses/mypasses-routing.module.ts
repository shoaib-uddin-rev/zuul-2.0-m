import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MypassesPage } from './mypasses.page';

const routes: Routes = [
  {
    path: '',
    component: MypassesPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MypassesPageRoutingModule {}
