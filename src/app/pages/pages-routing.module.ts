import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [

  {
      path: '',
      redirectTo: 'splash',
      pathMatch: 'full'
  },
  {
    path: 'splash',
    loadChildren: () => import('./splash/splash.module').then( m => m.SplashPageModule)
  },
  {
    path: 'login',
    loadChildren: () => import('./login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'totorial',
    loadChildren: () => import('./totorial/totorial.module').then( m => m.TotorialPageModule)
  },
  {
    path: 'signup',
    loadChildren: () => import('./signup/signup.module').then( m => m.SignupPageModule)
  },
  {
    path: 'forgot-password',
    loadChildren: () => import('./forgot-password/forgot-password.module').then( m => m.ForgotPasswordPageModule)
  },
  {
    path: 'dashboard',
    loadChildren: () => import('./dashboard/dashboard.module').then( m => m.DashboardPageModule)
  },
  {
    path: 'update-password',
    loadChildren: () => import('./update-password/update-password.module').then( m => m.UpdatePasswordPageModule)
  },
  {
    path: 'v-manage-vehicles',
    loadChildren: () => import('./v-manage-vehicles/v-manage-vehicles.module').then( m => m.VManageVehiclesPageModule)
  },
  {
    path: 'notifications',
    loadChildren: () => import('./notifications/notifications.module').then( m => m.NotificationsPageModule)
  },
  {
    path: 'parental-logs',
    loadChildren: () => import('./parental-logs/parental-logs.module').then( m => m.ParentalLogsPageModule)
  },
  {
    path: 'announcement',
    loadChildren: () => import('./announcement/announcement.module').then( m => m.AnnouncementPageModule)
  },
  {
    path: 'mypasses',
    loadChildren: () => import('./mypasses/mypasses.module').then( m => m.MypassesPageModule)
  },
  {
    path: 'request-a-pass',
    loadChildren: () => import('./request-a-pass/request-a-pass.module').then( m => m.RequestAPassPageModule)
  },
  {
    path: 'mypassesdetail',
    loadChildren: () => import('./mypassesdetail/mypassesdetail.module').then( m => m.MypassesdetailPageModule)
  },
  {
    path: 'contacts',
    loadChildren: () => import('./contacts/contacts.module').then( m => m.ContactsPageModule)
  },
  {
    path: 'sync-contacts',
    loadChildren: () => import('./sync-contacts/sync-contacts.module').then( m => m.SyncContactsPageModule)
  },
  {
    path: 'sync-contacts-status',
    loadChildren: () => import('./sync-contacts-status/sync-contacts-status.module').then( m => m.SyncContactsStatusPageModule)
  },
  {
    path: 'create-family',
    loadChildren: () => import('./create-family/create-family.module').then( m => m.CreateFamilyPageModule)
  },
  {
    path: 'country-code',
    loadChildren: () => import('./country-code/country-code.module').then( m => m.CountryCodePageModule)
  },
  {
    path: 'create-group',
    loadChildren: () => import('./create-group/create-group.module').then( m => m.CreateGroupPageModule)
  },
  {
    path: 'new-pass',
    loadChildren: () => import('./new-pass/new-pass.module').then( m => m.NewPassPageModule)
    // children: [
    //   { path: 'passform', component: PassformComponent},
    //   { path: 'passcontacts', component: PasscontactsComponent},
    //   { path: 'passendpage', component: PassendpageComponent},
    //   {
    //     path: '',
    //     redirectTo: 'passform',
    //     pathMatch: 'full'
    //   }
    // ]
  },

];


@NgModule({
    imports: [
      RouterModule.forChild(routes),
    ],
    exports: [RouterModule]
  })
  export class PagesRoutingModule { }
