import { Component, Injector, OnInit } from '@angular/core';
import { FormGroup, Validators } from '@angular/forms';
import { BasePage } from '../base-page/base-page';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.page.html',
  styleUrls: ['./signup.page.scss'],
})
export class SignupPage extends BasePage implements OnInit {

  aForm: FormGroup;
  submitAttempt = false;
  user: any;
  isfbSignup: boolean = false;
  isgSignup: boolean = false;
  isSocialSignup: boolean = false;
  hideForm: boolean = false;
  socialImage;
  formBuilder: any;


  constructor(
    injector: Injector
  ) {
    super(injector);
  }

  ngOnInit() {
    this.setupForm();

  }

  ionViewDidLoad() {
    // console.log('ionViewDidLoad SignupPage');
  }

  setupForm() {

    var re = /\S+@\S+\.\S+/;

    this.aForm = this.formBuilder.group({
      email: [''],
      name: ['shoaib', Validators.compose([Validators.minLength(3), Validators.maxLength(30), Validators.pattern('[a-zA-Z ]*'), Validators.required])],
      phone_number: ['3432322012', Validators.compose([ Validators.required]) ],
      password: ['Hotm@!l12', Validators.compose([Validators.minLength(9), Validators.maxLength(30), Validators.required, Validators.pattern(/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{6,}$/)])],
      password_confirm: ['Hotm@!l12', Validators.compose([Validators.minLength(6), Validators.maxLength(30), Validators.required])],
      profile_image: ['']
    }, { validator: this.utility.checkIfMatchingPasswords('password', 'password_confirm') })


  }

  login() {
    this.nav.setRoot('pages/login');
  }

  EsingUp(){

    this.submitAttempt = true;

    var in_name = !this.aForm.controls.name.valid || !this.utility.isLastNameExist(this.aForm.controls.name.value);
    var in_phone = !this.aForm.controls.phone_number.valid;
    in_phone = !this.utility.isPhoneNumberValid(this.aForm.controls.phone_number.value);
    var in_password = !this.aForm.controls.password.valid
    var in_cpassword = !this.aForm.controls.password_confirm.valid

    if(in_name){
      this.utility.presentFailureToast("Name/Full Name is required")
      return
    }


    console.log(in_phone);

    if(in_phone){
      this.utility.presentFailureToast("Phone Number required")
      return
    }

    if(in_password){
      this.utility.presentFailureToast("Valid Password Required")
      return
    }

    var _p = this.aForm.controls.password.value;
    var _cp = this.aForm.controls.password_confirm.value;


    if(_p != _cp){
      this.utility.presentFailureToast("confirm password must match password field")
      return
    }

    var formdata = this.aForm.value;
    formdata['profile_image'] = null;
    this.doSignup(formdata)

  }

  doSignup(formdata) {

    formdata['register_with_phonenumber'] = true;

    formdata['phone_number'] = parseInt(this.utility.getOnlyDigits(formdata['phone_number']));


    this.network.register(formdata).then(() => {
      formdata['showelcome'] = true;
      this.events.publish('user:login', formdata);
    }, err => {
      // console.log(err.message);
    });

  }

  onTelephoneChange(ev, tel) {

    console.log(tel)


    if (ev.inputType != "deleteContentBackward") {
      var _tel = this.utility.onkeyupFormatPhoneNumberRuntime(tel, false);
      // console.log(_tel);
      this.aForm.controls["phone_number"].setValue(_tel);
    }
  }

}
