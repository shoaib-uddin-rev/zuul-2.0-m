import { BasePage } from 'src/app/pages/base-page/base-page';
import { Component, OnInit, ViewChild, Injector } from '@angular/core';
import { IonSearchbar } from '@ionic/angular';
import { CreateEventPageComponent } from './create-event-page/create-event-page.component';
import { UserEventsService } from 'src/app/services/user-events.service';
import { ContactpopoverPageComponent } from 'src/app/components/contactpopover-page/contactpopover-page.component';

@Component({
  selector: 'app-other-events-page',
  templateUrl: './other-events-page.component.html',
  styleUrls: ['./other-events-page.component.scss'],
})
export class OtherEventsPageComponent extends BasePage implements OnInit {

  @ViewChild('searchbar', {static: true}) searchbar: IonSearchbar;
  search_value = '';
  search_on = false;
  offset = 1;
  events: any = [];
  selected_event: any = null;
  selected_item_id

  constructor(injector: Injector, public userEvents: UserEventsService) {
    super(injector);
  }

  ngOnInit() {
    this.getUserEvents(null, this.offset, false);
  }

  setFocusOnSearch(){

    var self = this;
    this.search_on = true;
    setTimeout( () => {
      self.searchbar.setFocus();
    }, 500);

  }

  filterGlobal($event){
    this.getUserEvents(this.search_value, 0, false);
  }

  async getUserEvents(search, offset, isConcat = true){

    return new Promise( resolve => {

      if(this.offset == -1){
        resolve(this.events);
        return;
      }

      this.userEvents.getUserEvents(search, offset).then((list: any) =>{

        console.log("isConcat", isConcat);
        if (isConcat == true) {
          this.events = this.events.concat(list);
        } else {
          this.events = list;
        }

        this.offset = list.length == 0 ? -1 : this.offset + 1;

        resolve(this.events);

      });

    });


  }

  resetSearch(){
    this.getUserEvents(null, 0, false);
  }

  doRefresh($event){

    this.userEvents.getUserEvents('', 1).then( list => {
      this.events = list;
      $event.complete();
    });


  }

  loadMoreContacts($event){
    this.getUserEvents(this.search_value, this.offset, true).then( v => {
      $event.target.complete();
    });
  }

  async presentPopover($event, item) {

    $event.stopPropagation();

    const _data = await this.popover.present(ContactpopoverPageComponent, $event, {
      id: item,
      flag: "E"
    });

    const data = _data.data;

    console.log(data)
    if (data.param == null) {
      return;
    }
    // var item = data["pid"];
    switch (data["param"]) {
      case "S":
        this.setPassEvent(item)
        break;
      case "E":
        this.plusHeaderButton($event, item);
        break;
      case "D":
        this.deleteEventFromList(item)
        break;
    }
  }

  async setPassEvent(item){
    // console.log(this.user_id);
    console.log(item);
    // await this.sqlite.setActiveEventInEvents(item.id);
    this.modals.dismiss(item);
  }

  async plusHeaderButton($event, item) {

    // CreateGroupPage as modal
    $event.stopPropagation();
    const _data = { event: item };
    const data = await this.modals.present(CreateEventPageComponent, _data);

    console.log(data);
    if(data["data"] != "A") {

      // var event = data["data"];
      // console.log(event);
      // var elementPos = this.events.map(function (x) { return x.id; }).indexOf(event.id);
      // // console.log(elementPos);
      // if(elementPos == -1){
      //   // event.new = true;
      //   // this.events.unshift(event);
      this.offset = 1;
      this.getUserEvents(null, 1, false).then( v => {
        // $event.target.complete();
      });

      // }else{
      //   this.events[elementPos] = event;
      // }
    }

  }

  async deleteEventFromList(item){

    let flag = await this.utility.presentConfirm('Agree', 'Disagree', 'Are You Sure?', 'This will hide this Event from your list but passes will not be effected' );
    if(flag){
        this.network.removeEventFromList(item['id']).then((res: any) => {

          this.sqlite.removeEventFromDatabase(item['id']);
          var elementPos = this.events.map(function (x) { return x.id; }).indexOf(item.id);
          this.events.splice(elementPos, 1);

        }, err => {});
    }
  }

  closeModal(res){
    this.modals.dismiss(res);
  }




}
