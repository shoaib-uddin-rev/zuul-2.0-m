import { ContactSearchListComponent } from './../../components/contact-search-list/contact-search-list.component';
import { NgModule } from '@angular/core';
import { CommonModule, Location } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { IonicModule } from '@ionic/angular';

import { PassformComponent } from './passform/passform.component';
import { PassendpageComponent } from './passendpage/passendpage.component';
import { PasscontactsComponent } from './passcontacts/passcontacts.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NewPassPage } from './new-pass.page';
import { OtherEventsPageComponent } from './other-events-page/other-events-page.component';
import { CreateEventPageComponent } from './other-events-page/create-event-page/create-event-page.component';
import { SharedModule } from 'src/app/components/shared.module';

@NgModule({
        declarations: [
                NewPassPage,
                PassformComponent,
                PassendpageComponent,
                PasscontactsComponent,
                OtherEventsPageComponent,
                CreateEventPageComponent
        ],
        imports: [
                CommonModule,
                IonicModule,
                FormsModule,
                ReactiveFormsModule,
                SharedModule

        ],
        exports: [
                NewPassPage
        ],
        providers: [
                Location
        ]
})
export class NewPassPageModule { }
