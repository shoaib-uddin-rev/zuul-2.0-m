import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PassendpageComponent } from './passendpage.component';

describe('PassendpageComponent', () => {
  let component: PassendpageComponent;
  let fixture: ComponentFixture<PassendpageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PassendpageComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PassendpageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
