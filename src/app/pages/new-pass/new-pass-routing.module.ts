import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NewPassPage } from './new-pass.page';
import { PasscontactsComponent } from './passcontacts/passcontacts.component';
import { PassendpageComponent } from './passendpage/passendpage.component';
import { PassformComponent } from './passform/passform.component';


const routes: Routes = [
    {
        path: '',
        component: NewPassPage,
        children: [
            { path: 'passform', component: PassformComponent },
            { path: 'passcontacts', component: PasscontactsComponent },
            { path: 'passendpage', component: PassendpageComponent },
            {
                path: '',
                redirectTo: 'passform',
                pathMatch: 'full'
            }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class SyncContactsPageRoutingModule { }
