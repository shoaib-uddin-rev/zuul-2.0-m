import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RequestAPassPage } from './request-a-pass.page';

const routes: Routes = [
  {
    path: '',
    component: RequestAPassPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RequestAPassPageRoutingModule {}
