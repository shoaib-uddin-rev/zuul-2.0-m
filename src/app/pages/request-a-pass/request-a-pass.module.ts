import { ContactSearchListComponent } from './../../components/contact-search-list/contact-search-list.component';
import { NgModule } from '@angular/core';
import { CommonModule, Location } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { IonicModule } from '@ionic/angular';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RequestAPassPageComponent } from '../request-apass-page/request-apass-page.component';
import { ChoiceOptionsComponent } from '../request-apass-page/choice-options/choice-options.component';
import { CommentOptionsComponent } from '../request-apass-page/comment-options/comment-options.component';
import { RequestSuccessComponent } from '../request-apass-page/request-success/request-success.component';


@NgModule({
        declarations: [
                RequestAPassPageComponent,
                ChoiceOptionsComponent,
                CommentOptionsComponent,
                RequestSuccessComponent
        ],
        imports: [
                CommonModule,
                IonicModule,
                FormsModule,
                ReactiveFormsModule


        ],
        exports: [
                RequestAPassPageComponent
        ],
        providers: [
                Location
        ]
})
export class RequestAPassPageModule {}
