import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TotorialPage } from './totorial.page';

const routes: Routes = [
  {
    path: '',
    component: TotorialPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TotorialPageRoutingModule {}
