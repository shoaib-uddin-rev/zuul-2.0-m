import { Component, Injector, OnInit, ViewChild } from '@angular/core';
import { IonSlides } from '@ionic/angular';
import { BasePage } from '../base-page/base-page';

@Component({
  selector: 'app-totorial',
  templateUrl: './totorial.page.html',
  styleUrls: ['./totorial.page.scss'],
})
export class TotorialPage extends BasePage implements OnInit {

  public dataArray = [
    {
      heading: 'Welcome to <br /> <strong class="text-secondary">ZUUL</strong>',
      subheading:
        'Zuul’s state of the art guest management system is designed specifically for your safety and convenience. Whether a resident or a guest, Zuul provides you with all the necessary tools for hassle-free guest registry and entry.',
      image: 'assets/imgs/tutorial.png',
    },
    {
      heading:
        'Contacts, Passes, <strong class="text-secondary">and Monitoring </strong>',
      subheading:
        'Easily import your contacts from your mobile device. Send passes to individuals or groups through a secure channel. Edit or cancel a pass with ease. Zuul’s mobile app gives you the ability to manage your passes anytime anywhere.',
      image: 'assets/imgs/tutorial-2.png',
    },
    {
      heading:
        'Highly Secure and <strong class="text-secondary">Convenient Guest Verification</strong>',
      subheading:
        'Having to dig around for your driver’s license at the gate is now a thing of the past. All passes display the guest’s ID, the sender’s information, and the QR code right on their mobile device.',
      image: 'assets/imgs/tutorial-3.png',
    },
    {
      heading:
        'Real-time Guest <strong class="text-secondary">Scanning and Notification</strong>',
      subheading:
        'Zuul is designed for everyone. Guards no longer need to waste time going back and forth into the guard house, long wait times at the gate are significantly reduced for guests, and residents can enjoy a convenient, easy to use guest management system.',
      image: 'assets/imgs/tutorial-4.png',
    },
  ];

  @ViewChild('slides', { static: true }) slides: IonSlides;
  showSkip = true;
  dir = 'ltr';

  constructor(injector: Injector) {
    super(injector);
  }

  ngOnInit() {}

  async goToNextSlide(slides) {
    const isEnd = await slides.isEnd();
    console.log(isEnd);
    if (isEnd) {
      this.signup();
    } else {
      slides.slideNext();
    }
  }
  openLoginPage() {
    this.nav.setRoot('pages/login');
  }
  signup() {
    this.nav.setRoot('pages/signup');
  }
  onSlideChangeStart(slider) {
    this.showSkip = !slider.isEnd();
  }

  ionViewDidLoad() {
    // console.log('ionViewDidLoad TutorialPage');
  }
}
