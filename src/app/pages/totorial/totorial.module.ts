import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TotorialPageRoutingModule } from './totorial-routing.module';

import { TotorialPage } from './totorial.page';
import { TutorialCardsComponentModule } from 'src/app/components/tutorial-cards/tutorial-cards-component.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TotorialPageRoutingModule,
    TutorialCardsComponentModule
  ],
  declarations: [TotorialPage]
})
export class TotorialPageModule {}
