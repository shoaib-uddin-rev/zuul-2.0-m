import { Component, Injector, Input, OnInit } from '@angular/core';
import { FormGroup, Validators } from '@angular/forms';
import { BasePage } from '../base-page/base-page';
import { CountryCodePage } from '../country-code/country-code.page';

const countries = {};

@Component({
  selector: 'app-create-family',
  templateUrl: './create-family.page.html',
  styleUrls: ['./create-family.page.scss'],
})

export class CreateFamilyPage extends BasePage implements OnInit {

  aForm: FormGroup;
  submitAttempt = false;
  canModify = false;
  user: any;
  phone_contacts;
  @Input() phone_number;
  @Input() name;
  isUpdate: boolean = false;
  btnText: string = "Create";
  user_id;
  @Input() contact_id;
  formattedNumber;
  @Input() show_relation = false;
  has_house: boolean = false;
  @Input() isFromRequestPassScreen: boolean = false;
  temporary_duration: number = 6;
  @Input() dial_code = { name: "United States", dial_code: "+1", code: "US", image: "assets/imgs/flags/us.png" };

  constructor(injector: Injector) {
    super(injector);

    this.setupForm();



  }

  ngOnInit() { }

  ngAfterViewInit() {
    this.sqlite.getActiveUser().then(u => {
      this.user = u;
      this.getMyContacts();
    });

    if (this.isFromRequestPassScreen == true) {
      this.btnText = "Request";
    }

    if (this.contact_id) {
      this.isUpdate = true;
      this.btnText = "Update";
      //console.log(this.contact_id);
      this.getContactInfo(this.contact_id);
    } else {

      this.canModify = true;
      var pn = this.phone_number;
      var ne = this.name;
      var co = this.dial_code.dial_code;

      if (ne) {
        this.aForm.controls['contact_name'].setValue(ne);
      }

      if (pn) {
        this.aForm.controls['phone_number'].setValue(pn);
      }

      if (co) {
        this.aForm.controls['dial_code'].setValue(co);
      }



    }
  }

  runTimeChange(ev) {
    let val = this.aForm.controls['phone_number'].value;
    this.formattedNumber = this.utility.formatPhoneNumberRuntime(val);
  };

  async openCounryCode() {
    // CreateGroupPage as modal
    const _data = await this.modals.present(CountryCodePage, { dc: this.dial_code })
    const data = _data.data;
    if (data['data'] != 'A') {
      this.dial_code = data;
      this.aForm.controls["dial_code"].setValue(this.dial_code.dial_code);
    }
  }

  setupForm() {

    var re = /\S+@\S+\.\S+/;

    this.aForm = this.formBuilder.group({
      contact_name: ['', Validators.compose([Validators.required]) /*, VemailValidator.checkEmail */],
      email: ['', Validators.compose([Validators.pattern(re), Validators.required])],
      // relationship: ['none', Validators.compose([Validators.required])],
      phone_number: ['', Validators.compose([Validators.required])],
      // can_manage_family: [false, Validators.compose([Validators.required])],
      // can_send_passes: [false, Validators.compose([Validators.required])],
      // allow_parental_control: [false, Validators.compose([Validators.required])],
      is_temporary: [false, Validators.compose([Validators.required])],
      temporary_duration: ['6'],
      dial_code: ['+1']
    })

  }

  onTelephoneChange(ev, tel) {

    if (ev.inputType != "deleteContentBackward") {
      var _tel = this.utility.onkeyupFormatPhoneNumberRuntime(tel, false);
      // console.log(_tel);
      this.aForm.controls['phone_number'].setValue(_tel);
    }

  }

  getContactInfo(contact_id) {

    var formdata = { contact_id: contact_id };
    console.log("Here is the contact Id", contact_id);
    this.network.getSingleContactById(contact_id).then((res: any) => {

      console.log(res);
      this.contact_id = res['id'];

      var contact_profile = res;
      this.user_id = contact_profile['user_id'];

      this.aForm.controls['contact_name'].setValue(contact_profile["contact_name"]);
      this.aForm.controls['email'].setValue(contact_profile["email"]);
      // contact_profile["relationship"] = (contact_profile["relationship"] != null) ? contact_profile["relationship"] : "none";
      // this.aForm.controls['relationship'].setValue(contact_profile["relationship"]);
      // this.formattedNumber = this.utility.formatPhoneNumber( contact_profile["phone_number"] );

      var _tel = this.utility.onkeyupFormatPhoneNumberRuntime(contact_profile["formatted_phone_number"]);
      this.aForm.controls['phone_number'].setValue(_tel);

      //this.aForm.controls['phone_number'].setValue( this.formattedNumber );
      // this.aForm.controls['can_manage_family'].setValue((contact_profile["its_user"]["can_manage_family"] == "1" ? true : false));
      // this.aForm.controls['can_send_passes'].setValue((contact_profile["its_user"]["can_send_passes"] == "1" ? true : false));
      // this.aForm.controls['allow_parental_control'].setValue((contact_profile["its_user"]["allow_parental_control"] == "1" ? true : false));

      this.canModify = contact_profile["modify"];
      this.aForm.controls['dial_code'].setValue(contact_profile["dial_code"]);

      // this.setDialCode(contact_profile["dial_code"])

    });

  }

  setDialCode(code) {
    this.sqlite.getCountriesInDatabase(null, 0, true).then((res: any) => {
      var n: any = res;
      console.log(n);
      this.dial_code = n.find(x => x.dial_code == code)
      console.log(res);
    }, error => {
      console.log(error);
    })
  }

  create() {


    // add validations
    var in_name = !this.aForm.controls.contact_name.valid || !this.utility.isLastNameExist(this.aForm.controls.contact_name.value)
    var in_email = (!this.aForm.controls.email.valid && this.show_relation == true)

    var in_phone = !this.aForm.controls.phone_number.valid
    var _validPhoneNumber = this.utility.getOnlyDigits(this.aForm.controls.phone_number.value);
    // console.log(_validPhoneNumber);
    if (_validPhoneNumber.toString().length < 10) {
      in_phone = true;
    } else {
      in_phone = false;
    }

    if (in_name) { this.utility.presentFailureToast("Please Enter Full Name"); return }

    if (in_phone) { this.utility.presentFailureToast("Please Enter Valid Phone Number"); return }
    if (in_email && this.show_relation) { this.utility.presentFailureToast("Please Enter Email"); return }

    if (this.aForm.controls.email.value != "" && in_email) { this.utility.presentFailureToast("Please Enter Email"); return }

    if (this.isFromRequestPassScreen == true) {
      // check if a user is already on zuul

      this.network.checkIfCellNumberUserCanSentPass(_validPhoneNumber).then((res: any) => {

        var returnObj = { isContactExist: true, data: res['user'], isFromRequestPassScreen: this.isFromRequestPassScreen };
        // if(this.aForm.controls.is_temporary.value == true){
        this.createFamilyMember()
          .then(data => {
            returnObj['contact'] = data['contact'];
            this.closeModal(returnObj)
          }, err => {
            // console.log(err);
          })
        // }else{
        //   this.closeModal(returnObj)
        // }

      }, err => { })


    } else {
      this.createFamilyMember()
        .then(data => {
          this.closeModal(data);
        }, err => {
          // console.log(err);
        })
    }




  }

  createFamilyMember() {

    return new Promise((resolve, reject) => {

      this.submitAttempt = true;
      var formdata = this.aForm.value;
      formdata['is_update'] = this.isUpdate;

      if (this.isUpdate) {
        formdata['contact_id'] = this.contact_id;
        formdata['user_id'] = this.user_id;
      }

      // formdata['is_family'] = this.show_relation;

      console.log(formdata);

      this.network.createFamilyMember(formdata).then((res: any) => {
        // this.utility.presentSuccessToast(res.message);
        resolve(res);


      }, err => {
        reject(err);
      });


    })



  }

  closeModal(res) {
    this.modals.dismiss(res);
  }

  getMyContacts() {

    this.utility.getPhoneContacts()
      .then(data => {
        // console.log(data);
        this.phone_contacts = data;
      })
  }

  openList() {
    this.utility.getSinglePhoneContact()
      .then(number => {
        // console.log("s-2")
        // console.log(number);
        this.aForm.controls['phone_number'].setValue(number);

      })
  }



}
