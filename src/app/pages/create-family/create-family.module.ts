import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CreateFamilyPageRoutingModule } from './create-family-routing.module';

import { CreateFamilyPage } from './create-family.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CreateFamilyPageRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [CreateFamilyPage]
})
export class CreateFamilyPageModule {}
