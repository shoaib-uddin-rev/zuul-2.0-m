import { Component, Injector, OnInit } from '@angular/core';
import { FormGroup, Validators } from '@angular/forms';
import { BasePage } from '../base-page/base-page';
import { ContactpopoverPageComponent } from '../../components/contactpopover-page/contactpopover-page.component';
import { ForgotPasswordPage } from '../forgot-password/forgot-password.page';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage extends BasePage implements OnInit {

  aForm: FormGroup;
  submitAttempt = false;
  sw_user: any = null;
  navwithin: boolean = false;


  constructor(
    injector: Injector
  ) {
    super(injector);
    this.setupForm();
    // this.setUserInForm(this.nav.getParams());
  }

  ngOnInit() {
    this.setupForm();
  }




  setupForm() {

    var re = /\S+@\S+\.\S+/;

    this.aForm = this.formBuilder.group({
      phone_number: ['3432322008', Validators.compose([Validators.required]) /*, VemailValidator.checkEmail */],
      password: ['Hotm@!l12', Validators.compose([Validators.minLength(3), Validators.maxLength(30), Validators.required])]
    })

  }



  openLogUsers(myEvent) {
    // var r = item;
    var self = this;
    let popover2 = this.popover.present( ContactpopoverPageComponent, {
      id: null,
      flag: "SW"
    });

    // popover2.present({
    //   ev: myEvent
    // });

    // popover2.onDidDismiss(data => {
    //   // let tr = data;
    //   if (data == null) {
    //     return;
    //   }

    //   var sw_user = data["param"];
    //   // console.log(sw_user);
    //   this.setUserInForm(sw_user)

    // });
  }

  setUserInForm(sw_user) {
    if (sw_user) {
      this.users.switchUserAccount(sw_user).then(data => {
        if (!data) {
          this.navwithin = true;
          this.aForm.controls['phone_number'].setValue(sw_user['phone_number'])
        }
      });


    }
  }

  onTelephoneChange(ev, tel) {
    if (ev.inputType != "deleteContentBackward") {
      let _tel = this.utility.onkeyupFormatPhoneNumberRuntime(tel, false);
      this.aForm.controls["phone_number"].setValue(_tel);
    }
  }

  forgetpassword() {
    let myModal = this.modals.present(ForgotPasswordPage);
  }

  login() {
    this.submitAttempt = true;
    const formdata = this.aForm.value;

    formdata.registerWithPhonenumber = true;
    this.events.publish('user:login', formdata);
  }

  signup() {
    this.nav.setRoot('pages/signup');
  }

}
