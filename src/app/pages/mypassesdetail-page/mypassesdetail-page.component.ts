import { Component, Injector, Input, OnInit } from '@angular/core';
import { BasePage } from '../base-page/base-page';
import { GmapPageComponent } from '../gmap-page/gmap-page.component';
import { ManageVehiclePageComponent } from '../manage-vehicle-page/manage-vehicle-page.component';

@Component({
  selector: 'app-mypassesdetail-page',
  templateUrl: './mypassesdetail-page.component.html',
  styleUrls: ['./mypassesdetail-page.component.scss'],
})
export class MypassesdetailPageComponent extends BasePage implements OnInit {

  vehicle: any;
  is_vehicle = false;

  _item;
  @Input() set item(value: any) {
    this.network.getPasseDetails(value).then(res =>{
      // console.log(res);
      this._item = res['pass'];
      this.getPassVehicles();
    }, err => {});
    
  }

  get item(): any {
    return this._item;
  }

  constructor(injector: Injector) { 
    super(injector);
  }

  ngOnInit() {}

  async getPassVehicles(){

    let obj = {
      pass_id: this.item['pass_id'],
      qrcode_id: this.item['qrcode_id']
    }

    this.network.getPassVehicle(obj).then( async res => {
      console.log(res);
      if(res['vehicle']){
        this.vehicle = res['vehicle'];
        this.is_vehicle = true;
      }else{

        this.is_vehicle = false;
        await this.utility.showAlert("Please select your vehicle")
        this.setPassVehicles()

      }


    })
  }

  async setPassVehicles(){

    // open vehicle modal with CRUD
    const _data = await this.modals.present(ManageVehiclePageComponent, {from_new_pass: true});
    const data = _data.data;


    console.log(data);
    if(data['data'] != 'A'){
      this.vehicle = data['data'];

      let obj = {
        pass_id: this.item['pass_id'],
        qrcode_id: this.item['qrcode_id'],
        vehicle_id: this.vehicle['id'],
      }

      if(this.item['visitor_type'] == 2){
        obj['vendor_id'] = this.item['vendor_id'];
      }


      // console.log(obj);
      this.network.setPassVehicle(obj).then( res => {
        // console.log(res);
        this.is_vehicle =  true;
      })

    }


  }


  callMe(num){
    this.utility.dialMyPhone(num);
  }

  async getDirection(address){
    const addr = address.trim();
    const data = this.modals.present(GmapPageComponent, {address: addr, newAddress: true, isDirections: true});
  }

  async addToContact(item){

    const flag = await this.utility.presentConfirm('Agree', 'Disagree', 'Add to Contacts?', 'I Would  Like to Add <strong>' + item.sender + '</strong> to ZUUL Contacts');

    if(flag){
      this.network.addContactWithOnlyId(item.contact_id).then( res => {
        this.utility.presentSuccessToast(res['message'])
        if(res['bool'] == true){
          this.item.contact_id = 0;
        }
      }, err => {});
    }

  }

  addareminder(item){

    const detailObj = {
      event_name: item.event_name,
      description: item.description,
      notes: item.phone ? "Call at: " + item.phone : "",
      startDate: Date.parse(item.to_date + ' ' + item.to_date_time),
      endDate: Date.parse(item.for_date + ' ' + item.for_date_time)
    }
    this.utility.addaremondertodate(detailObj);
  }

  closeModal(){
    this.modals.dismiss();
  }

}
