import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { VManageVehiclesPage } from './v-manage-vehicles.page';

const routes: Routes = [
  {
    path: '',
    component: VManageVehiclesPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class VManageVehiclesPageRoutingModule {}
