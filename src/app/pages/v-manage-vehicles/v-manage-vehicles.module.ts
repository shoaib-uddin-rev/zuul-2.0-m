import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { VManageVehiclesPageRoutingModule } from './v-manage-vehicles-routing.module';

import { VManageVehiclesPage } from './v-manage-vehicles.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    VManageVehiclesPageRoutingModule
  ],
  declarations: [VManageVehiclesPage]
})
export class VManageVehiclesPageModule {}
