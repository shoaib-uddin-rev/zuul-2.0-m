import { Component, Injector, OnInit } from '@angular/core';
import { FormGroup, Validators } from '@angular/forms';
import { StoredContactsService } from 'src/app/services/stored-contacts.service';
import { BasePage } from '../base-page/base-page';

@Component({
  selector: 'app-create-group',
  templateUrl: './create-group.page.html',
  styleUrls: ['./create-group.page.scss'],
})
export class CreateGroupPage extends BasePage implements OnInit {

  aForm: FormGroup;
  submitAttempt = false;
  phone_contacts: any;
  return_data: any;
  storedData;
  it;
  it_group;
  user: any;

  constructor(
    injector: Injector,
    private storedcontacts: StoredContactsService
  ) {
    super(injector)
    this.setupForm();
  }

  ngOnInit() {}

  ngAfterViewInit() {
    this.initialize();
    this.sqlite.getActiveUser().then(u => {
      this.user = u;
      this.getMyContacts();
    });

  }

  initialize() {

    console.log(this.it);

    if (this.it) {

      this.it_group = this.it;
      this.aForm.controls['group_name'].setValue(this.it_group['group_name']);
      this.aForm.controls['description'].setValue(this.it_group['description']);
      // this.network.getContactGroup(this.it.id).then(res => {
      //   this.it_group = res['group'];
      //   this.aForm.controls['group_name'].setValue(this.it_group['group_name']);
      //   this.aForm.controls['description'].setValue(this.it_group['description']);
      // }, err => { })

    }

  }
  getMyContacts() {

    this.utility.getPhoneContacts()
      .then(data => {
        // console.log(data);
        this.phone_contacts = data;
      })
  }


  setupForm() {

    this.aForm = this.formBuilder.group({
      group_name: ['', Validators.compose([Validators.required]) /*, VemailValidator.checkEmail */],
      description: ['']
    })

  }

  create() {

    var in_name = !this.aForm.controls.group_name.valid;

    if (in_name) {
      this.utility.presentFailureToast("group name is required")
      return
    }

    this.submitAttempt = true;
    var formdata = this.aForm.value;

    this.network.createContactGroup(formdata).then((res: any) => {
      this.utility.presentSuccessToast(res['message'])
      this.closeModal(res);

    }, err => { });

  }

  update() {

    var in_name = !this.aForm.controls.group_name.valid;

    if (in_name) {
      this.utility.presentFailureToast("group name is required")
      return
    }

    this.submitAttempt = true;
    var formdata = this.aForm.value;

    console.log(formdata);

    // formdata["description"] = "description";
    
    this.network.updateContactGroup(this.it.id, formdata).then((res: any) => {
      this.closeModal(res);
    }, err => { });

  }

  closeModal(res) {
    this.modals.dismiss(res);
  }

}
