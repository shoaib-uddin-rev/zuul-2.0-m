import { Component, Injector, OnInit } from '@angular/core';
import { BasePage } from '../base-page/base-page';

@Component({
  selector: 'app-contacts',
  templateUrl: './contacts.page.html',
  styleUrls: ['./contacts.page.scss'],
})
export class ContactsPage extends BasePage implements OnInit {

  tab1Root = 'contacts-list';
  tab2Root = 'group-list';
  tab3Root = 'favorite-list';

  constructor(
    injector: Injector
  ) { 
    super(injector)
    this.events.subscribe('contacts:popallpages', (time) => {
      // user and time are the same arguments passed in `events.publish(user, time)`
      this.nav.pop();
    });
  }

  ngOnInit() {}

}
