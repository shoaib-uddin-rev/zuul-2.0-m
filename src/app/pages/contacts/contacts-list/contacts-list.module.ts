import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ContactsListPageRoutingModule } from './contacts-list-routing.module';

import { ContactsListPage } from './contacts-list.page';
import { SharedModule } from './../../../components/shared.module'

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ContactsListPageRoutingModule,
    SharedModule
  ],
  declarations: [ContactsListPage]
})
export class ContactsListPageModule { }
