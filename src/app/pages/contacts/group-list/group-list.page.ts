import { Component, Injector, OnInit } from '@angular/core';
import { StoredContactsService } from 'src/app/services/stored-contacts.service';
import { BasePage } from '../../base-page/base-page';
import { GroupContactListPage } from '../group-contact-list/group-contact-list.page';

@Component({
  selector: 'app-group-list',
  templateUrl: './group-list.page.html',
  styleUrls: ['./group-list.page.scss'],
})
export class GroupListPage extends BasePage implements OnInit {

  constructor(injector: Injector, public storedcontacts: StoredContactsService) {
    super(injector);
    this.storedcontacts.getMyGroups(true);
  }

  ngOnInit() { }

  showGroupCOntacts(item) {
    let extras = {
      queryParams: { group_id: item['id'], group_name: item['group_name'], description: item['description'] }
    }
    this.nav.navigateTo('/contacts/group-contact-list', extras);
  }

  goBack() {
    this.events.publish('contacts:popallpages', Date.now());
  }

  plusHeaderButton(param) {
    this.storedcontacts.createContactGroup();
  }

}
