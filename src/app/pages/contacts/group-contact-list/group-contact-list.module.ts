import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { GroupContactListPageRoutingModule } from './group-contact-list-routing.module';

import { GroupContactListPage } from './group-contact-list.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    GroupContactListPageRoutingModule
  ],
  declarations: [GroupContactListPage]
})
export class GroupContactListPageModule {}
