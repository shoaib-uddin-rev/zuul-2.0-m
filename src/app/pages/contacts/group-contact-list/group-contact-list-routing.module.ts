import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { GroupContactListPage } from './group-contact-list.page';

const routes: Routes = [
  {
    path: '',
    component: GroupContactListPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class GroupContactListPageRoutingModule {}
