import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ContactsPage } from './contacts.page';

const routes: Routes = [
  {
    path: '',
    component: ContactsPage,
    children: [
      {
        path: 'contacts-list',
        loadChildren: () => import('./contacts-list/contacts-list.module').then( m => m.ContactsListPageModule)
      },
      {
        path: 'favorite-list',
        loadChildren: () => import('./favorite-list/favorite-list.module').then( m => m.FavoriteListPageModule)
      },
      {
        path: 'group-contact-list',
        loadChildren: () => import('./group-contact-list/group-contact-list.module').then( m => m.GroupContactListPageModule)
      },
      {
        path: 'group-list',
        loadChildren: () => import('./group-list/group-list.module').then( m => m.GroupListPageModule)
      },
      {
        path: '',
        redirectTo: 'contacts-list',
        pathMatch: "full"
      },
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ContactsPageRoutingModule {}
