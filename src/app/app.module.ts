import { ErrorHandler, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
import { FCM } from '@ionic-native/fcm/ngx';
import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { HttpClient, HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AppErrorHandlerService } from './services/app-error-handler.service';
import { FirebaseX } from '@ionic-native/firebase-x/ngx';
import { EventsService } from './services/events.service';
import { ApiService } from './services/api.service';
import { StorageService } from './services/basic/storage.service';
import { NetworkService } from './services/network.service';
import { SqliteService } from './services/sqlite.service';
import { LocationStrategy, PathLocationStrategy } from '@angular/common';
import { Calendar } from '@ionic-native/calendar/ngx';
import { CallNumber } from '@ionic-native/call-number/ngx';
import { Camera } from '@ionic-native/camera/ngx';
import { Contacts } from '@ionic-native/contacts/ngx';
import { DatePicker } from '@ionic-native/date-picker/ngx';
import { ImagePicker } from '@ionic-native/image-picker/ngx';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { LaunchNavigator } from '@ionic-native/launch-navigator/ngx';
import { NativeStorage } from '@ionic-native/native-storage/ngx';
import { OpenNativeSettings } from '@ionic-native/open-native-settings/ngx';
import { SQLite } from '@ionic-native/sqlite/ngx';
import { NgxPubSubService } from '@pscoped/ngx-pub-sub';
import { InterceptorService } from './services/interceptor.service';
import { PagesModule } from './pages/pages.module';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [
    BrowserModule, 
    IonicModule.forRoot(), 
    AppRoutingModule,
    HttpClientModule,
    PagesModule,
  ],
  providers:
  [
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    { provide: ErrorHandler, useClass: AppErrorHandlerService },
    FirebaseX,
    { provide: HTTP_INTERCEPTORS, useClass: InterceptorService, multi: true },
    SQLite,
    Calendar,
    ImagePicker,
    LaunchNavigator,
    CallNumber,
    InAppBrowser,
    OpenNativeSettings,
    Camera,
    DatePicker,
    Geolocation,
    Contacts,    
    SplashScreen,
    StatusBar,
    NativeStorage,
    FCM,
    // custom providers
    NgxPubSubService,
    EventsService,
    SqliteService,
    StorageService,
    ApiService,
    NetworkService,
    Location,
    { provide: LocationStrategy, useClass: PathLocationStrategy },    

 ],
  bootstrap: [AppComponent],
})
export class AppModule {}
