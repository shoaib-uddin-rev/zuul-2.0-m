import { Injectable } from '@angular/core';

const states = require('src/app/data/states.json');

@Injectable({
  providedIn: 'root'
})
export class StateService {

  constructor() { }

  getStates() {
    return states;
  }

}
