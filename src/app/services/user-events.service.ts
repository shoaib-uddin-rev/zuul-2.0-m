import { Injectable } from '@angular/core';
import { NetworkService } from './network.service';

@Injectable({
  providedIn: 'root'
})
export class UserEventsService {

  storedData = {
    search: '',
    page: 1
  };

  constructor(public network: NetworkService) {

  }

  getUserEvents(search, page){

    this.storedData.search = search;
    this.storedData.page = page;

    return new Promise( resolve => {
      this.network.getUserEvents(this.storedData).then( v => {
        console.log(v);
        resolve(v.list);
      });
    });


  }

}
