export const browserDBInstance = (db) => ({
    executeSql: (sql, data) => new Promise((resolve, reject) => {
        db.transaction(
          (tx) => {
            tx.executeSql(sql, data, (tx, rs) => {
              resolve(rs);
            });
          },
          (err) => reject(err)
        );
      }),
    sqlBatch: (arr) => new Promise((r, rr) => {
        const batch = [];
        db.transaction((tx) => {
          for (let i = 0; i < arr.length; i++) {
            batch.push(
              new Promise((resolve) => {
                tx.executeSql(arr[i][0], arr[i][1], (tx, rs) => {
                  resolve(rs);
                });
              })
            );
            Promise.all(batch).then(() => r(true));
          }
        });
      }),
  });

import { SQLiteDatabaseConfig } from '@ionic-native/sqlite';

export class SQLiteObject {
  db: any;

  constructor(db: any) {
    this.db = db;
  }

  executeSql(queryStatement: string, params: any): Promise<any> {
    return new Promise((resolve, reject) => {
      this.db.transaction((tx) => {
        tx.executeSql(
          queryStatement,
          params,
          (tx, result) => {
            resolve(result);
          },
          (error) => reject(error)
        );
      });
    });
  }
}

export class SQLiteMock {
  public create(config: SQLiteDatabaseConfig): Promise<SQLiteObject> {
    const db = (<any>window).openDatabase(
      'mydb',
      '',
      'my first database',
      2 * 1024 * 1024
    );

    return new Promise((resolve, reject) => {
      resolve(new SQLiteObject(db));
    });
  }
}
