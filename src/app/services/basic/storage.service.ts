import { Injectable } from '@angular/core';
import { NativeStorage } from '@ionic-native/native-storage/ngx';

@Injectable({
  providedIn: 'root'
})
export class StorageService {

  constructor(private nativeStorage: NativeStorage) { }

  set(key, data): Promise<boolean>{

    return new Promise( resolve => {

      localStorage.setItem(key, data);
      resolve(true);
      // this.nativeStorage.setItem(key, data)
      // .then(
      //   () => resolve(true),
      //   error => resolve(false)
      // );
    });

  }

  get(key): Promise<any>{

    return new Promise( resolve => {

      const data = localStorage.getItem(key);
      resolve(data);

      // this.nativeStorage.getItem(key)
      // .then(
      //   data => resolve(data),
      //   error => resolve(null)
      // );
    });

  }

}
