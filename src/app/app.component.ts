import { Component } from '@angular/core';
import { Platform, MenuController } from '@ionic/angular';
import { AudioService } from './services/audio.service';
import { EventsService } from './services/events.service';
import { NavService } from './services/nav.service';
import { NetworkService } from './services/network.service';
import { SqliteService } from './services/sqlite.service';
import { UserService } from './services/user.service';
import { UtilityService } from './services/utility.service';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { FCM } from '@ionic-native/fcm/ngx';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {

  canBeResident = false;
  canShowSettings = true;
  isEmailVerificationPending = false;
  user_role_id = -1;

  pages: any[] = [
    { title: 'Become Resident', component: 'CodeVerificationPage' },
    { title: 'Manage Family', component: 'MyfamilymembersPage' },
    { title: 'Settings', component: 'SettingsPage' },
    { title: 'Edit Profile', component: 'RegistrationPage' },
    { title: 'Logout', method: 'logout' },
    { title: 'Activity Logs', component: 'ActivityLogsPage' },
    { title: 'Zuul Key', component: 'ZKeyPage' },
    { title: 'Manage Vehicles', component: 'VManageVehiclesPage' },
  ];

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private fcm: FCM,
    public menuCtrl: MenuController,
    private events: EventsService,
    private sqlite: SqliteService,
    public userService: UserService,
    public utilityProvider: UtilityService,
    public users: UserService,
    // public permission: PermissionsService,
    // private storage: Storage,
    public audio: AudioService,
    public network: NetworkService,
    public nav: NavService
  ) {
    platform.ready().then(() => {

      menuCtrl.enable(false, 'authenticated');
      statusBar.styleDefault();
      if (platform.is('cordova') || platform.is('ios') || platform.is('android')) {
        this.initializeApp();

      }
    });

    // assign events
    this.assignEvents();
  }

  initializeApp() {

    this.platform.ready().then( async () => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      this.audio.preload();
      await this.sqlite.initialize();
      this.setupFMC();
      this.initializeCountries();
    });

  }

  initializeCountries() {

  }

  setupFMC() {

    this.fcm.subscribeToTopic('all');
    this.fcm.onNotification().subscribe(data => {
      if (!data.wasTapped) {
        this.audio.play('');
        if (data.showalert != null) {
          this.events.publish('user:shownotificationalert', data);
        } else {
          this.events.publish('user:shownotification', data);
        }
      };
    });
    this.fcm.onTokenRefresh().subscribe(token => {
      this.sqlite.setFcmToken(token);
      this.events.publish('user:settokentoserver');
    });

  }

  async getFCMToken() {
    return new Promise(resolve => {
      this.fcm.getToken().then(v => resolve(v), (err) => { console.log(err); resolve(null); }).catch(v => resolve(null));
    });
  }



  login(user) {

    console.log(user);

    const showelcome = user.showelcome;

    const emailData = {
      email: user.email,
      password: user.password,
    };

    const phoneData = {
      phone_number:  this.utilityProvider.getOnlyDigits(user.phone_number),
      password: user.password,
      registerWithPhonenumber: 1
    };

    const data = (user.registerWithPhonenumber === true) ? phoneData : emailData;

    console.log(data);

    this.network.login(phoneData).then(async res => {
      console.log(res);

      if (res.user.role_id === 8) {
        this.utilityProvider.presentToast('Guard / Security Personnal can\'t login here');
        this.logout();
        return;
      }

      this.menuCtrl.enable(true, 'authenticated');
      const token = res.success.token;

      res.user.token = token;
      res.user.active = 1;

      console.log('processUserData', res.user);
      await this.processUserData(res.user, showelcome);
    }, err => { });

  }

  logout() {
    this.menuCtrl.enable(false, 'authenticated');
    this.sqlite.setLogout();
    this.nav.setRoot('pages/totorial');
  }

  setSuccessPage(params) {
    //// console.log(params);
    this.menuCtrl.enable(true, 'authenticated');
    this.nav.setRoot('SuccessPage', params);
  }

  notificationReceivedalert(data) {
    // // console.log(data.showalert);
    if (data.hasOwnProperty('showalert')) {
      // this.rnotif = true;
      this.utilityProvider.showAlert(data.showalert).then(() => {
        if (data.showalert == 'Pass scanned successfully') {
          this.nav.setRoot('pages/dashboard', {
            animate: true,
            direction: 'backword'
          });
        }
      });
    }



  }

  openPage(page) {


    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.menuCtrl.close();
    if (page.hasOwnProperty('component')) {

      if (page.component == 'VManageVehiclesPage') {
        this.events.publish('dashboard:carselection');
        return;
      }

      if (page.component == 'RegistrationPage') {
        this.events.publish('dashboard:updateprofile');
        return;
      }

      this.nav.push(page.component, { revisit: true });
      return;

    }
    if (page.hasOwnProperty('method')) {
      this[page.method]();
    }


  }

  private assignEvents() {
    this.events.subscribe('user:logout', this.logout.bind(this));
    this.events.subscribe('user:login', this.login.bind(this));
    this.events.subscribe('user:get', this.getUser.bind(this));
    this.events.subscribe('user:successpage', this.setSuccessPage.bind(this));
    this.events.subscribe('user:settokentoserver', this.setTokenToServer.bind(this));
    this.events.subscribe('user:shownotificationalert', this.notificationReceivedalert.bind(this));
    this.events.subscribe('user:setcontactstodatabase', this.setContacts.bind(this));

  }

  private getUser() {

    this.network.getUser().then(async (res: any) => {
      console.log(res);
      this.processUserData(res.user, false);
    }, err => {
      this.logout();
    });
  }

  private async processUserData(user, showelcome) {

    // check if sqlite set already, if not fetch records

    const fcmToken = await this.getFCMToken();
    user.fcm_token = fcmToken;

    // let _user = user["result"];
    console.log('setUserInDatabase', user);
    console.log(user);
    const saveduser = await this.sqlite.setUserInDatabase(user);
    this.menuCtrl.enable(true, 'authenticated');

    console.log(saveduser);

    if (!saveduser) {
      this.logout();
      return;
    }

    // this.userService.setUser(user);
    // this.canBeResident = (parseInt(saveduser.can_user_become_resident) == 1);
    // this.canShowSettings = parseInt(saveduser["role_id"]) != 7

    const currentUrl = this.nav.router.url;
    console.log(currentUrl);

    if (currentUrl === 'pages/dashboard') {
      this.events.publish('dashboard:initialize');
    } else {
      this.nav.setRoot('pages/dashboard',
        {
          showelcome,
          animate: true,
          direction: 'forward'
        }
      );
    }




  }

  private async setContacts(user) {

    // this.users.getContactDatabseOfUser(user).then(v => {
    //   console.log('import completed', v);
    // });
  }


  private async setTokenToServer() {
    const fcmToken = await this.getFCMToken();
    // console.log(fcm_token);
    if (fcmToken) {
      this.network.saveFcmToken({ token: fcmToken }).then(dats => {
      }, err => { console.error(err); });
    }

  }



}
